#include <stdio.h>

void num_swap(char* a)
{
    printf("sizeof: %d\n", sizeof(a));
    printf("strlen: %d\n", strlen(a));
}

int main(int argn, char* argc[])
{
    char a[50] = {0}; 
    for (int i = 0; i < 1; i++)
    {
        printf("\n Test Result\n");
        num_swap(a);
    }

    return 0;
}