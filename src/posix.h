#define _GNU_SOURCE 1

#include <stdio.h>
#include <sched.h>
#include <pthread.h>
#include <mqueue.h>
#include <semaphore.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <stdint.h>
#include <signal.h>
#include <fcntl.h>
#include <errno.h>
#include <limits.h>
#include <iCunit.h>
#include <sys/utsname.h>
#include <sys/wait.h>
#include <unistd.h>

#define MAXMSG5 5
#define MSGLEN 10
#define MAXMSG 10

const int MQUEUE_SHORT_ARRAY_LENGTH  = 10; // = strlen(MQUEUE_SEND_STRING_TEST)
const int MQUEUE_STANDARD_NAME_LENGTH  = 50;

#define MQUEUE_NO_ERROR 0
#define MQUEUE_IS_ERROR (-1)
#define MQUEUE_PTHREAD_PRIORITY_TEST1 3
#define MQUEUE_PTHREAD_PRIORITY_TEST2 4
#define MQUEUE_PATH_MAX_TEST PATH_MAX
#define MQUEUE_NAME_MAX_TEST NAME_MAX
#define MQUEUE_SEND_STRING_TEST "0123456789"
#define MQUEUE_PTHREAD_NUM_TEST 5
#define MQUEUE_PRIORITY_TEST 0
#define MQUEUE_TIMEOUT_TEST 7
#define MQUEUE_PRIORITY_NUM_TEST 3
#define MQUEUE_MAX_NUM_TEST (LOSCFG_BASE_IPC_QUEUE_CONFIG - QUEUE_EXISTED_NUM)
#define MQ_MAX_MSG_NUM 16
#define MQ_MAX_MSG_LEN 64
#define HWI_NUM_TEST 1
#define HWI_NUM_TEST1 2
#define LOS_WAIT_FOREVER 0XFFFFFFFF
#define MQ_VALID_MAGIC 0x6db256c1
const int  LOSCFG_BASE_IPC_QUEUE_CONFIG = 1024;

#ifdef __LP64__
#define PER_ADDED_VALUE 8
#else
#define PER_ADDED_VALUE 4
#endif

typedef unsigned int TSK_HANDLE_T;

char *g_mqueueMsessage[MQUEUE_SHORT_ARRAY_LENGTH] = {"0123456789", "1234567890", "2345678901", "3456789012", "4567890123", "5678901234", "6789012345", "7890123456", "lalalalala", "hahahahaha"};
char g_gqname[MQUEUE_STANDARD_NAME_LENGTH];
char g_mqueueName[LOSCFG_BASE_IPC_QUEUE_CONFIG + 1][MQUEUE_STANDARD_NAME_LENGTH];
mqd_t g_mqueueId[LOSCFG_BASE_IPC_QUEUE_CONFIG + 1];
mqd_t g_messageQId;
mqd_t g_gqueue;