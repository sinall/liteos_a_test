# RECORD

1. sem
   + linux
     + result
        ```
        setting up affinity 0
        setting up prio 20
        -- Sem: Wait block --
        max=20651
        min=1302
        average=1525
        -- Sem: Signal unblock --
        max=20238
        min=1241
        average=1503
        ```

   + liteos_a
     + result
        ```
        setting up affinity 0
        setting up prio 20
        -- Sem: Wait block --
        max=584100
        min=32180
        average=47813
        -- Sem: Signal unblock --
        max=688880
        min=22060
        average=38310

        Test End
        ```

2. sem_workload
   + linux
     + result
        ```
        setting up affinity 0
        setting up prio 20
        -- Sem workload --
        max=38858
        min=2573
        average=12384

        Test end

        ```
   + liteos_a
     + result
        ```
        OHOS:/$ ./bin/sem_workload     
        setting up affinity 0
        setting up prio 20
        -- Sem workload --
        max=3254680
        min=76340
        average=247203

        ```

3. sem_processing
   + linux
     + result
        ```
        setting up affinity 0
        setting up prio 20
        --- Sem: Signal ---
        max=185
        min=23
        average=40
        --- Sem: Wait ---
        max=20804
        min=24
        average=100

        Test end
        ```
   + liteos_a
     + result
        ```
        OHOS:/$ ./bin/sem_processing
        setting up affinity 0
        setting up prio 20
        --- Sem: Signal ---
        max=562240
        min=4300
        average=6750
        --- Sem: Wait ---
        max=646860
        min=6840
        average=9134

        Test end
        ```

4. sem_prio
   + linux
     + result
        ```
        setting up affinity 0
        setting up prio 20
        --- sem singaling with prio ---
        max=18199
        min=870
        average=1836

        ```
   + liteos_a
     + result
        ```
        setting up affinity 0
        setting up prio 20
        --- sem singaling with prio ---
        max=639500
        min=14620
        average=21268

        ```