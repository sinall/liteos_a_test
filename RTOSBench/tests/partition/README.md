# RECORD

**Attention** : I find some interesting behaviours when i run the process with different paraments.when you create some thread to increase the cpu workload to get the jitter in a high load condition you think.but the jitter size is not change like which you predict. the jitter will be smaller when it has some brother thread to increase CPU workload than the single monitor thread has been created. its very interesting but i dont know why. maybe i should search on the website for an answer or get the answer from the kernel code.

1. jitter(which has brother threads)
   - linux
     - result
       ```
       setting up affinity 0
       setting up prio 20
       6544
       532
       5053
       5767
       5163
       5500
       6417
       5193
       6031
       20122
       5658
       5056
       9328
       4846
       5061
       5259
       5413
       6714
       6393
       ```
       
   - liteos_a
     - result
       ```
       OHOS:/$ ./bin/jitter
       setting up affinity 0
       setting up prio 20
       219580
       118440
       122020
       137760
       201680
       141420
       140040
       140700
       117540
       297880
       118180
       173340
       309780
       188720
       ```
