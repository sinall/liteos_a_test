/* Copyright (c) 2019, Guillaume Champagne
 * All rights reserved.
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree.
 */

#include "porting_layer.h"

#define NB_ITER 1000000
#define NB_TASK 2
#define MQ_LEN 1
#define MQ_MSGSIZE 50

#define MQ_NAME "/12345"
#define MQ_MSGSEND "abced"

no_task_retval_t mq_initialize_test(no_task_argument_t args);
no_task_retval_t sender(no_task_argument_t args);
no_task_retval_t receiver(no_task_argument_t args);

no_task_handle_t tasks_handle[NB_TASK];
no_mq_t mq;

DECLARE_TIME_COUNTERS(no_time_t, s_to_r)
DECLARE_TIME_COUNTERS(no_time_t, r_to_s)

no_main_retval_t main(no_main_argument_t args)
{
	no_initialize_test(mq_initialize_test);
	return MAIN_DEFAULT_RETURN;
}

no_task_retval_t mq_initialize_test(no_task_argument_t args)
{

	/* Create resources */
	int32_t i;
	no_task_retval_t (*thread_array[NB_TASK])(no_task_argument_t) = {receiver, sender};

	no_mq_destory(MQ_NAME);
	no_mq_create(&mq, MQ_NAME, MQ_LEN, MQ_MSGSIZE);

	for (i = 0; i < NB_TASK; i++)
	{
		tasks_handle[i] = no_create_task(thread_array[i], NULL, BASE_PRIO + i);
	}

	for (i = 0; i < NB_TASK; i++)
	{
		no_join_task(tasks_handle[i]);
	}

	no_serial_write("\nTest end!\n");
	no_mq_destory(MQ_NAME);

	return TASK_DEFAULT_RETURN;
}

no_task_retval_t sender(no_task_argument_t args)
{
	int32_t i;

	/* 2b - Benchmark. */
	for (i = 0; i < NB_ITER + 1; i++)
	{
		no_mq_send(mq, MQ_MSGSEND);
		WRITE_T2_COUNTER(r_to_s)
	}

	for (i = 0; i < NB_ITER; i++)
	{
		WRITE_T1_COUNTER(s_to_r)
		no_mq_send(mq, MQ_MSGSEND);
	}

	no_task_suspend_self();

	return TASK_DEFAULT_RETURN;
}

no_task_retval_t receiver(no_task_argument_t args)
{
	int32_t i;

	DECLARE_TIME_STATS(int64_t)

	/* 1 - Let sender start */
	no_mq_receive(mq);

	/* 2a - Benchmark. */
	for (i = 0; i < NB_ITER; i++)
	{
		WRITE_T1_COUNTER(r_to_s)
		no_mq_receive(mq);
		COMPUTE_TIME_STATS(r_to_s, i);
	}

	REPORT_BENCHMARK_RESULTS("-- MQ: Receive block --")
	RESET_TIME_STATS()

	for (i = 0; i < NB_ITER; i++)
	{
		no_mq_receive(mq);
		WRITE_T2_COUNTER(s_to_r)
		COMPUTE_TIME_STATS(s_to_r, i)
	}

	REPORT_BENCHMARK_RESULTS("-- MQ: Signal unblock --")

	no_task_suspend_self();

	return TASK_DEFAULT_RETURN;
}
